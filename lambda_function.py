import urllib.request
import shutil
from datetime import datetime

from util.general import date2name
from util.aws_s3 import upload_file, connect_s3

s3_resource = connect_s3()


def lambda_handler(event, context):
    # get full htmls and from there filter out our the VEH0134-URL
    file_url = 'https://download.geofabrik.de/europe/britain-and-ireland-latest.osm.pbf'
    file_type = 'britain-and-ireland-latest'

    # upload to S3
    date = datetime.now().date()
    date_folder = date2name(date, format='YYYY_MX')
    folder = '1_RawData/Geo_OpenStreetMaps/' + date_folder + '/'
    bucket = 'ev-modeling-platform'
    key = folder + file_type.lower() + '.osm.pbf'

    download_to_s3(file_url, s3_resource, bucket, s3_location=key)


def get_html(url):
    response = urllib.request.urlopen(url)
    data = response.read()  # a `bytes` object
    html = data.decode(
        'utf-8')  # a `str`; this step can't be used if data is binary
    return html


def download_to_s3(url, s3_resource, s3_bucket, s3_location):
    response = urllib.request.urlopen(url)
    uploadByteStream = response.read()  # bytes object
    upload_file(s3_resource, s3_location, body=uploadByteStream)
